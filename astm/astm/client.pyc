ó
DDQc           @   sO  d  d l  Z  d  d l Z d d l m Z d d l m Z d d l m Z m Z d d l	 m
 Z
 d d l m Z d d l m Z e  j e  Z d	 d
 g Z d e f d     YZ i d g d 6d d d g d 6d d d g d 6d d d d d g d 6d d d d d g d 6d g d 6d g d 6Z d
 e f d     YZ d	 e f d     YZ d S(   iÿÿÿÿNi   (   t   loop(   t   encode(   t   ENQt   EOT(   t   NotAccepted(   t   Record(   t   ASTMProtocolt   Clientt   Emittert   RecordsStateMachinec           B   s)   e  Z d  Z d   Z d   Z d   Z RS(   s!  Simple state machine to track emitting ASTM records in right order.

    :param mapping: Mapping of the ASTM records flow order.
                    Keys should be string and defines record type, while values
                    expected as sequence of other record types that may be used
                    after current one.
                    For example: ``{"H": ["P", "C", "L"]}`` mapping defines that
                    if previous record had ``"H"`` type, then the next one
                    should have ``"P"``, ``"C"`` or ``"L"`` type or
                    :exc:`AssertionError` will be raised. The default mapping
                    reflects common ASTM records flow rules. If this argument
                    specified as :const:`None` no rules will be applied.
    :type: dict
    c         C   s   | |  _  d  |  _ d  S(   N(   t   mappingt   Nonet   state(   t   selfR
   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyt   __init__'   s    	c         C   sK   | d  k	 r> |  j |  s> t d | |  j |  j f   n  | |  _ d  S(   Ns%   invalid state %r, expected one of: %r(   R   t   is_acceptablet   AssertionErrorR
   R   (   R   R   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyt   __call__+   s
    c         C   sL   |  j  d  k r t S| |  j  k r& t S|  j  |  j } d | k pK | | k S(   Nt   *(   R
   R   t   Truet   FalseR   (   R   R   t
   next_types(    (    s&   /usr/lib/python3.7/astm/astm/client.pyR   2   s    (   t   __name__t
   __module__t   __doc__R   R   R   (    (    (    s&   /usr/lib/python3.7/astm/astm/client.pyR	      s   		t   Ht   Ct   Pt   Lt   Ot   RR   c           B   s\   e  Z d  Z e Z d e d  Z d d  Z d   Z	 d d  Z
 d d d  Z d   Z RS(   s8  ASTM records emitter for :class:`Client`.

    Used as wrapper for user provided one to provide proper routines around for
    sending Header and Terminator records.

    :param emitter: Generator/coroutine.

    :param encoding: Data encoding.
    :type encoding: str

    :param flow_map: Records flow map. Used by :class:`RecordsStateMachine`.
    :type: dict

    :param chunk_size: Chunk size in bytes. If :const:`None`, emitter record
                       wouldn't be split into chunks.
    :type chunk_size: int

    :param bulk_mode: Sends all records for single session (starts from Header
                      and ends with Terminator records) via single message
                      instead of sending each record separately. If result
                      message is too long, it may be split by chunks if
                      `chunk_size` is not :const:`None`. Keep in mind, that
                      collecting all records for single session may take some
                      time and server may reject data by timeout reason.
    :type bulk_mode: bool
    c         C   sa   |   |  _  t |  _ | |  _ |  j |  |  _ t |  _ d |  _ g  |  _ | |  _	 | |  _
 d  S(   Ni    (   t   _emitterR   t
   _is_activet   encodingt   state_machinet
   records_smt   emptyt   last_seqt   buffert
   chunk_sizet	   bulk_mode(   R   t   emittert   flow_mapR!   R'   R(   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyR   f   s    						c         C   s   |  j  j |  j r | n d   } |  j s6 t |  _ n  t | t  rT | j   } n  y |  j | d  Wn, t	 k
 r } |  j
 t |  | j  n X| S(   Ni    (   R   t   sendR    R   R   t
   isinstanceR   t   to_astmR#   t	   Exceptiont   throwt   typet   args(   R   t   valuet   recordt   err(    (    s&   /usr/lib/python3.7/astm/astm/client.pyt   _get_recordt   s    !	c         C   s  |  j  rj | g } x: t rN |  j t  } | j |  | d d k r Pq q Wt | |  j |  j  } n0 |  j d 7_ t | g |  j |  j |  j  } |  j j	 |  |  j j
 d  } |  j t |  j  7_ | d d k r d |  _ |  j j t  n  | S(   Ni    R   i   (   R(   R   R5   t   appendR   R!   R'   R%   R&   t   extendt   popt   lenR   (   R   R3   t   recordst   chunkst   data(    (    s&   /usr/lib/python3.7/astm/astm/client.pyt   _send_record   s$    				c         C   s;   |  j  r | r |  j  j d  S|  j |  } |  j |  S(   sB  Passes `value` to the emitter. Semantically acts in same way as
        :meth:`send` for generators.

        If the emitter has any value within local `buffer` the returned value
        will be extracted from it unless `value` is :const:`False`.

        :param value: Callback value. :const:`True` indicates that previous
                      record was successfully received and accepted by server,
                      :const:`False` signs about his rejection.
        :type value: bool

        :return: Next record data to send to server.
        :rtype: bytes
        i    (   R&   R8   R5   R=   (   R   R2   R3   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyR+      s    c         C   s5   |  j  j | | |  } | d k	 r1 |  j |  Sd S(   sß   Raises exception inside the emitter. Acts in same way as
        :meth:`throw` for generators.

        If the emitter had catch an exception and return any record value, it
        will be proceeded in common way.
        N(   R   R/   R   R=   (   R   t   exc_typet   exc_valt   exc_tbR3   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyR/   ®   s    c         C   s   |  j  j   d S(   sN   Closes the emitter. Acts in same way as :meth:`close` for generators.
        N(   R   t   close(   R   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyRA   ¹   s    N(   R   R   R   R	   R"   R   R   R   R5   R=   R+   R/   RA   (    (    (    s&   /usr/lib/python3.7/astm/astm/client.pyR   F   s   	c           B   s   e  Z d  Z e Z d d d d e d e d  Z d   Z	 d   Z
 d   Z e d  Z d	 d
  Z d   Z d   Z d   Z d   Z d   Z d   Z RS(   s
  Common ASTM client implementation.

    :param emitter: Generator function that will produce ASTM records.
    :type emitter: function

    :param host: Server IP address or hostname.
    :type host: str

    :param port: Server port number.
    :type port: int

    :param timeout: Time to wait for response from server. If response wasn't
                    received, the :meth:`on_timeout` will be called.
                    If :const:`None` this timer will be disabled.
    :type timeout: int

    :param flow_map: Records flow map. Used by :class:`RecordsStateMachine`.
    :type: dict

    :param chunk_size: Chunk size in bytes. :const:`None` value prevents
                       records chunking.
    :type chunk_size: int

    :param bulk_mode: Sends all records for single session (starts from Header
                      and ends with Terminator records) via single message
                      instead of sending each record separately. If result
                      message is too long, it may be split by chunks if
                      `chunk_size` is not :const:`None`. Keep in mind, that
                      collecting all records for single session may take some
                      time and server may reject data by timeout reason.
    :type bulk_mode: bool

    Base `emitter` is a generator that yield ASTM records one by one preserving
    their order::

        from astm.records import (
            HeaderRecord, PatientRecord, OrderRecord, TerminatorRecord
        )
        def emitter():
            assert (yield HeaderRecord()), 'header was rejected'
            ok = yield PatientRecord(name={'last': 'foo', 'first': 'bar'})
            if ok:  # you also can decide what to do in case of record rejection
                assert (yield OrderRecord())
            yield TerminatorRecord()  # we may do not care about rejection

    :class:`Client` thought :class:`RecordsStateMachine` keep track
    on this order, raising :exc:`AssertionError` if it is broken.

    When `emitter` terminates with :exc:`StopIteration` or :exc:`GeneratorExit`
    exception client connection to server closing too. You may provide endless
    `emitter` by wrapping function body with ``while True: ...`` loop polling
    data from source from time to time. Note, that server may have communication
    timeouts control and may close session after some time of inactivity, so
    be sure that you're able to send whole session (started by Header record and
    ended by Terminator one) within limited time frame (commonly 10-15 sec.).
    t	   localhosti`;  i   c	   	   
   C   s   t  t |   j d |  |  j t j t j  |  j | | f  |  j | d | pZ |  j	 d | d | d | |  _
 d |  _ d  S(   Nt   timeoutR!   R*   R'   R(   i   (   t   superR   R   t   create_sockett   sockett   AF_INETt   SOCK_STREAMt   connectt   emitter_wrapperR!   R)   t
   terminator(	   R   R)   t   hostt   portR!   RC   R*   R'   R(   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyR   ý   s    c         C   s!   t  t |   j   |  j   d S(   s%   Initiates ASTM communication session.N(   RD   R   t   handle_connectt   _open_session(   R   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyRN     s    c         C   s$   |  j  j   t t |   j   d  S(   N(   R)   RA   RD   R   t   handle_close(   R   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyRP     s    c         C   s   |  j  t  d  S(   N(   t   pushR   (   R   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyRO     s    c         C   s$   |  j  t  | r  |  j   n  d  S(   N(   RQ   R   t   close_when_done(   R   t   close_connection(    (    s&   /usr/lib/python3.7/astm/astm/client.pyt   _close_session  s    g      ð?c         O   s   t  | | |  d S(   sg   Enters into the :func:`polling loop <astm.asynclib.loop>` to let
        client send outgoing requests.N(   R    (   R   RC   R1   t   kwargs(    (    s&   /usr/lib/python3.7/astm/astm/client.pyt   run  s    c         C   s   t  d   d S(   s&   Raises :class:`NotAccepted` exception.s   Client should not receive ENQ.N(   R   (   R   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyt   on_enq"  s    c         C   sa   y |  j  j t  } Wn t k
 r6 |  j t  n' X|  j |  | t k r] |  j   n  d S(   s   Handles ACK response from server.

        Provides callback value :const:`True` to the emitter and sends next
        message to server.
        N(   R)   R+   R   t   StopIterationRT   RQ   R   RO   (   R   t   message(    (    s&   /usr/lib/python3.7/astm/astm/client.pyt   on_ack&  s    c         C   s   |  j  t k r |  j t  Sy |  j j t  } Wn> t k
 rR |  j t  nG t	 k
 rr |  j t    n' X|  j |  | t
 k r |  j   n  d S(   sæ   Handles NAK response from server.

        If it was received on ENQ request, the client tries to repeat last
        request for allowed amount of attempts. For others it send callback
        value :const:`False` to the emitter.N(   t   _last_sent_dataR   RQ   R)   R+   R   RX   RT   R   R.   R   RO   (   R   RY   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyt   on_nak5  s    c         C   s   t  d   d S(   s&   Raises :class:`NotAccepted` exception.s   Client should not receive EOT.N(   R   (   R   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyt   on_eotJ  s    c         C   s   t  d   d S(   s&   Raises :class:`NotAccepted` exception.s'   Client should not receive ASTM message.N(   R   (   R   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyt
   on_messageN  s    c         C   s$   t  t |   j   |  j t  d S(   sB   Sends final EOT message and closes connection after his receiving.N(   RD   R   t
   on_timeoutRT   R   (   R   (    (    s&   /usr/lib/python3.7/astm/astm/client.pyR_   R  s    N(   R   R   R   R   RJ   R   t   DEFAULT_RECORDS_FLOW_MAPR   R   RN   RP   RO   RT   RV   RW   RZ   R\   R]   R^   R_   (    (    (    s&   /usr/lib/python3.7/astm/astm/client.pyR   ¿   s    8									(   t   loggingRF   t   asynclibR    t   codecR   t	   constantsR   R   t
   exceptionsR   R
   R   t   protocolR   t	   getLoggerR   t   logt   __all__t   objectR	   R   R`   R   R   (    (    (    s&   /usr/lib/python3.7/astm/astm/client.pyt   <module>
   s(   #

y